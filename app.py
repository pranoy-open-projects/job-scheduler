from flask import Flask, jsonify, request
from flask_restful import Api, Resource
from pymongo import MongoClient

app = Flask(__name__)
api = Api(app)

client = MongoClient("mongodb://localhost:27017/taskDB")
db = client.SimilarityDB
tasks = db["Tasks"]

dctStatus = {
    -1: 'Failed',
    0: 'Unscheduled',
    1: 'Pending',
    2: 'Running',
    3: 'Success'
}


def taskExists(taskName):
    if tasks.find({"taskName": taskName}).count() == 0:
        return False
    return True


def taskScheduled(taskName):
    if tasks.find({"taskName": taskName, 'status': {"$in": [1, 2]}}).count() == 0:
        return False
    return True

class Task(Resource):

    def post(self):
        data = request.get_json()
        time1 = data['time1']
        time2 = data['time2']
        taskname = data['taskName']
        jobName = data['jobName']
        if taskExists(taskname):
            response = {
                'status': 409,
                'msg': "Task Already exists"
            }
            return jsonify(response)

        try:
            tasks.insert({
                "taskName": taskname,
                "time1": time1,
                "time2": time2,
                "jobName": jobName,
                'lastRun': None,
                'lastSuccessfulRun': None,
                'totalFailures': 0,
                'totalRuns':0,
                'status': 0
            })

            response = {
                'status': 200,
                "msg": "Task added successfully"
            }
            return jsonify(response)
        except Exception as e:
            response = {
                'status': 502,
                'msg': "Document insertion failed"
            }
            return jsonify(response)

    def get(self):
        # Find all tasks in the db
        allTasks = tasks.find({})
        data = []
        for task in allTasks:
            data.append({
                'taskName': task['taskName'],
                'jobName': task['jobName'],
                'lastSuccessfulRun': task['lastSuccessfulRun'],
                'lastRun': task['lastRun'],
                'totalFailures': task['totalFailures'],
                'totalRuns': task['totalRuns'],
                'status': dctStatus[task['status']]
            }
            )
        return jsonify(data)

    def delete(self):
        data = request.get_json()
        taskName = data['taskName']
        if not taskExists(taskName):
            response = {
                'status': 404,
                'msg': "Task Does not exist"
            }
            return jsonify(response)

        try:
            tasks.delete_one({'taskName':taskName})

            response = {
                'status': 200,
                "msg": "Task deleted successfully"
            }
            return jsonify(response)
        except Exception as e:
            response = {
                'status': 502,
                'msg': "Document deletion failed"
            }
            return jsonify(response)

class Schedule(Resource):
    def post(self):
        data = request.get_json()
        taskname = data['taskName']
        # Validate whether the job is scheduled
        if taskScheduled(taskname):
            response = {
                'status': 409,
                'msg': "Task Already Scehduled/Running"
            }
            return jsonify(response)

        try:
            # Schedule a job(Change job status to pending)
            tasks.update({
                "taskName": taskname,
            }, {
                "$set": {
                    'node': "Unassigned",
                    'cores': "Unknown",
                    'status': 1
                }
            })

            response = {
                'status': 200,
                "msg": "Task Scheduled successfully"
            }
            return jsonify(response)
        except Exception as e:
            response = {
                'status': 502,
                'msg': "Document updation failed"
            }
            return jsonify(response)

    def get(self):
        # Find schedules tasks in the db
        scheduledTasks = tasks.find({'status': {"$in": [1, 2]}})
        data = []
        for task in scheduledTasks:
            data.append({
                'taskName': task['taskName'],
                'jobName': task['jobName'],
                'lastSuccessfulRun': task['lastSuccessfulRun'],
                'lastRun': task['lastRun'],
                'totalFailures': task['totalFailures'],
                'totalRuns': task['totalRuns'],
                'node': task['node'],
                'cores': task['cores'],
                'status': dctStatus[task['status']]
            }
            )
        return jsonify(data)


# Define the URL's
api.add_resource(Task, '/task')
api.add_resource(Schedule, '/schedule')


if __name__ == "__main__":
    app.run(debug=True)
