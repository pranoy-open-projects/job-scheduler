# Job Scheduler

Scheduler is a combination of 2 application
1) Serve API's for creating, viewing, scheduling and deleting tasks
2) Process the jobs that are queued to be processed

## Setup



##### 1. Setup Environment with the packages given in requirements.txt


##### 2. export FLASK_APP=app.py


##### 3. Run the applications

api         : Flask run
scheduler   : python process_scheduler.py

#### API's Being served:
 
##### Endpoint1: Task (post, get, delete) : /task

post: input = {
    "taskName": "task1",
    "time1": 1, 
    "time2": 1,
    "jobName": "longRunningJob",
        }

delete : input = {
    "taskName" : "task1" 
                    }

##### Endpoint2: Schedule (post, get)         : /schedule

post: input = {
    "taskName" : "task1" 
                    }
#### Parameters for the scheduler

Scheduler accepts 1 parameter which is the time gap between concurrent db calls to check for pending jobs.

-t or --time

use: python process_scheduler.py --help for info





#### Viable Further Improvements:

1) Add a Date and time field to the job while scheduling and make sure the job is only taken into the queue if it is after that time
2) Schedule jobs based on frequency(weekly, biweekly, daily, monthly)
3) Create proper Logs for the jobs
4) Connect app to a mail server to update the corresponding users with the status of the job
5) Create a scheduled job which checks whether the timed jobs are completed on time and mark them as late running if needed
6) Take job/task info from a csv file and bulk import/export task data. Can be used for easy backup and restore




