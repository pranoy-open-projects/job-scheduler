from tasks import *
import concurrent.futures
from pymongo import MongoClient
import functools
import optparse
from datetime import datetime
import time
import platform
import math


def get_arguments():
    parser = optparse.OptionParser()
    parser.add_option("-d", "--delay", dest="delay",
                      help="Time gap after which the scheduler should ping the MongoDB server again in seconds")
    (options, arguments) = parser.parse_args()
    if not options.delay:
        print("[-] Warning: No delay time given, default value is 1 seconds")
    return options


class ProcessScheduler:

    # Establishing connection with mongodb
    client = MongoClient("mongodb://localhost:27017/taskDB")
    db = client.SimilarityDB
    tasks = db["Tasks"]

    def __init__(self, timeGap=1):
        self.timeGap = timeGap
        self.scheduledTasks = []
    # Maps the string form of jobname with its corresponding function

    def longRunningEval(self, jobname, **kwargs):
        function = f'{jobname}({jobname},args={kwargs})'
        eval(function)

    # Callback function being attached to the future
    def updateDb(self, *args):
        fut = args[1]
        taskname = args[0]
        try:
            # Checking whether the job has completed(Throws error if not completed)
            fut.result()
            # Update status of the job to 3(Success) and increment total runs by 1
            ProcessScheduler.tasks.update_one(
                {"taskName": taskname}, {
                    "$set": {
                        'status': 3,
                        "lastSuccessfulRun": datetime.now()
                    },
                    "$inc": {
                        'totalRuns': 1
                    }
                }, )
            self.task = ProcessScheduler.tasks.find_one({'taskName': taskname})
        # Updates the task status to -1(Failed) if some error occurs
        except Exception as e:
            print(f'[-] {taskname} Failed.')
            ProcessScheduler.tasks.update_one(
                {"taskName": taskname}, {
                    "$set": {
                        'status': -1
                    },
                    "$inc": {
                        'totalRuns': 1,
                        'totalFailures': 1
                    }
                })

    def mainLoop(self):
        # machine
        machine = platform.machine()

        # node
        node = platform.node()

        # processor
        with open("/proc/cpuinfo", "r") as f:
            info = f.readlines()

        cpuinfo = [x.strip().split(":")[1] for x in info if "model name" in x]
        frequency = (cpuinfo[0].split(" ")[-1])
        cores = len(cpuinfo)

        # Memory
        with open("/proc/meminfo", "r") as f:
            lines = f.readlines()
        totalMem = int(lines[0].split(" ")[-2])
        usedMem = int(lines[1].split(" ")[-2])

        memoryPerc = round(usedMem/totalMem*100)
        #Assuming load taken by the cpu is going to be 50%
        #Could also be parameterized(High: 1, low: .5, medium: .7)
        cpuUtilization = .7
        #Average ratio of wait time to compute time of job (Assumed)
        #Could make a calculated field based on the average time of
        #the completed jobs start, end time and cores recorded in the DB
        AvgRWT2CT = 2
        optimalPoolSize = math.floor(cores * cpuUtilization * AvgRWT2CT)
        
        print(f'Node:       {node}')
        print(f'Machine:    {machine}')
        print(f'Cores:      {cores}')
        print(f'Frequncy:   {frequency}')
        print(f"Memory :    {memoryPerc}%")
        print(f'Calculated Pool size:  {optimalPoolSize}')

        # Commented snippet for testing with dummy data

        # ProcessScheduler.tasks.drop()
        # ProcessScheduler.tasks.insert_one({'taskName': 'Task1',
        #                                    'jobName': 'longRunningJob',
        #                                    'status': 1,
        #                                    'lastRun': None,
        #                                    'lastSuccessfulRun': None,
        #                                    'totalFailures': 0,
        #                                    'totalRuns': 0,
        #                                    'status': 1})
        # for document in ProcessScheduler.tasks.find():
        #     print(document)
        with concurrent.futures.ThreadPoolExecutor(max_workers=optimalPoolSize) as executor:
            # Ping the mongodb continuosly and check for pending tasks until interrupted by the user
            while True:
                print("[+] Checking Database for new jobs")
                self.scheduledTasks = ProcessScheduler.tasks.find(
                    {'status': 1})
                # print(self.pendingTaks)
                for task in self.scheduledTasks:
                    # Setting the status of the job to 2(Running)
                    ProcessScheduler.tasks.update_one(task, {
                        "$set": {
                            "status": 2,
                            "node": node,
                            "cores": cores,
                            "lastRun": datetime.now()
                        }
                    })
                    # Submitting the job
                    args = {
                        'time1': 1,
                        'time2': 2,
                    }
                    future = executor.submit(
                        self.longRunningEval, task['jobName'], args=args)
                    # Attaching a callback function to the future(Executes when the job is done)
                    future.add_done_callback(
                        functools.partial(self.updateDb, task['taskName']))
                        
                time.sleep(self.timeGap)


if __name__ == "__main__":
    arguments = get_arguments()
    if arguments.delay:
        processScheduler = ProcessScheduler(timeGap=int(arguments.delay))
    else:
        processScheduler = ProcessScheduler()

    processScheduler.mainLoop()
