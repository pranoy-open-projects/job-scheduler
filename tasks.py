import time


def longRunningJob(jobname, **kwargs):
    time.sleep(kwargs.get('time1', 1))
    print(f'[+] {jobname} running.')
    time.sleep(kwargs.get('time2',1))
    print(f'[+] {jobname} completed.')

def longRunningJob2(jobname, **kwargs):
    time.sleep(kwargs.get('time1', 1))
    print(f'[+] {jobname} running.')
    time.sleep(kwargs.get('time2',1))
    print(f'[+] {jobname} completed.')